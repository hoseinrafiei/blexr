<?php
namespace Blexr\SportOdds\Blocks;

use Blexr\SportOdds\Blocks;
use Blexr\SportOdds\Interfaces\BlockType;
use Blexr\SportOdds\Plugin;

/**
 * Odds Block of WordPress Block Editor
 * @package Blexr\SportOdds\Blocks
 */
class Odds extends Blocks implements BlockType
{
    /**
     * @var string
     */
    public string $name = 'blexr/sport-odds';

    /**
     * @var string
     */
    public string $nonce = 'blexr-sport-odds-nonce';

	public function __construct()
    {
        parent::__construct();
	}

    /**
     * Register Hooks
     */
    public function hooks()
    {
        add_action('init', function()
        {
            // Block Type
            $this->register();

            // Styles
            $this->styles();

            // Scripts
            $this->scripts();
        });

        // AJAX Request
        add_action('wp_ajax_bs_ajax_odds', [$this, 'filter']);
        add_action('wp_ajax_nopriv_bs_ajax_odds', [$this, 'filter']);
    }

    /**
     * Register Block Type in WordPress Block Editor
     */
    public function register()
    {
        // Block Prefix
        $prefix = $this->slug();

        register_block_type($this->name, [
            'script' => $prefix,
            'style' => $prefix,
            'editor_script' => $prefix.'-editor',
            'editor_style' => $prefix.'-editor',
            'render_callback' => [$this, 'render'],
        ]);
    }

    /**
     * Register Block Styles
     */
    public function styles()
    {
        // Block Prefix
        $prefix = $this->slug();

        // Block Editor Style
        wp_register_style($prefix.'-editor', Plugin::url() . '/assets/dist/css/block-odds-editor.css', [], Plugin::version(), false);

        // Frontend Style
        wp_register_style($prefix, Plugin::url() . '/assets/dist/css/block-odds.css', [], Plugin::version(), false);
    }

    /**
     * Register Block Scripts
     */
    public function scripts()
    {
        // Block Prefix
        $prefix = $this->slug();

        wp_register_script($prefix, Plugin::url() . '/assets/dist/js/block-odds.min.js', ['jquery'], Plugin::version(), true);

        wp_register_script($prefix.'-editor', Plugin::url() . '/assets/dist/js/block-odds-editor.min.js', [
            'wp-editor',
            'wp-blocks'
        ], Plugin::version(), true);

        wp_localize_script($prefix.'-editor', 'blexr', [
            'ajax' => admin_url('admin-ajax.php'),
            'nonce' => $this->createNonce(),
            'name' => $this->name,
            'title' => __('Odds Table', 'blexr-sport-odds'),
            'description' => __('It display a responsive table showing top 10 odds', 'blexr-sport-odds')
        ]);
    }

    /**
     * Render Output of Block
     * @return string
     */
    public function render()
    {
        // API Object
        $api = $this->getAPI();

        // Sports
        $sports = $api->getSports();

        // Markets
        $markets = $api->getMarkets();

        // Regions
        $regions = $api->getRegions();

        // Unique ID
        $uid = uniqid();

        // Nonce Name
        $nonce = $this->nonce;

        // jQuery Plugin
        $this->footer($uid);

        return $this->view()->make('odds', compact('sports', 'markets', 'regions', 'uid', 'nonce'))->render();
    }

    /**
     * Search Odds and Generate Table
     */
    public function filter()
    {
        // Check if security nonce is set
        if(!isset($_GET['_wpnonce'])) $this->JSONResponse(['success' => 0, 'message' => esc_html__("Security nonce is required.", 'blexr-sport-odds')]);

        // Verify that the nonce is valid
        if(!wp_verify_nonce(sanitize_text_field($_GET['_wpnonce']), $this->nonce)) $this->JSONResponse(['success' => 0, 'message' => esc_html__("Security nonce is invalid.", 'blexr-sport-odds')]);

        // Verify the Referer
        if(!$this->CheckAjaxReferer()) $this->JSONResponse(['success' => 0, 'message' => esc_html__("Referer is invalid!", 'blexr-sport-odds')]);

        // Params
        $sport = (isset($_GET['sport']) ? sanitize_text_field($_GET['sport']) : NULL);
        $region = (isset($_GET['region']) ? sanitize_text_field($_GET['region']) : NULL);
        $market = (isset($_GET['market']) ? [sanitize_text_field($_GET['market'])] : []);

        // Missing Required Parameters
        if(!$sport or !$region) $this->JSONResponse(['success' => 0, 'message' => esc_html__("Sport and region parameters are required.", 'blexr-sport-odds')]);

        // API Object
        $api = $this->getAPI();
        $odds = $api->getOdds($sport, [$region], $market);

        // Limit the Results
        if(is_array($odds)) $odds = array_slice($odds, 0, 10);
        else $odds = [];

        // Generate Table
        $output = $this->view()->make('odds-table', compact('odds'))->render();

        // Send Response
        $this->JSONResponse([
            'success' => 1,
            'output' => $output
        ]);
    }

    /**
     * Initialize jQuery Plugin
     * @param $uid
     */
    public function footer($uid)
    {
        add_action('wp_footer', function() use($uid)
        {
            echo '<script>
            jQuery(document).ready(function()
            {
                jQuery("#bs-odds-'.$uid.'").BlexrOddsTable(
                {
                    id: "'.$uid.'",
                    ajax_url: "'.admin_url('admin-ajax.php', NULL).'"
                });
            });
            </script>';
        }, 9999);
    }
}