<?php
namespace Blexr\SportOdds;

use Blexr\SportOdds\Interfaces\Initiable;

/**
 * Class Plugin
 * @package Blexr\SportOdds
 */
final class Plugin extends Base implements Initiable
{
	public function __construct()
    {
        parent::__construct();
	}

    /**
     * Add Activation, Deactivation and Uninstall Hooks
     */
    public function init()
    {
        // Plugin Activation Hook
        register_activation_hook(BSO_FILEPATH, [$this, 'activate']);

        // Plugin Deactivation Hook
        register_deactivation_hook(BSO_FILEPATH, [$this, 'deactivate']);

        // Plugin Uninstall Hook
        register_uninstall_hook(BSO_FILEPATH, [Plugin::class, 'uninstall']);
    }

    /**
     * Runs on plugin activation
     */
    public function activate()
    {
    }

    /**
     * Runs on plugin deactivation
     */
    public function deactivate()
    {
    }

    /**
     * Runs on plugin un-installation
     */
    public static function uninstall()
    {
        // Remove the plugin options
        delete_option('blexr_odds_options');
    }

    /**
     * Check PHP Minimum Requirements
     * @return bool
     */
    public static function meetRequirements()
    {
        if(!version_compare(PHP_VERSION, '7.4.0', '>=')) return false;
        return true;
    }

    /**
     * Absolute path of plugin
     * @return string
     */
    public static function abspath()
    {
        return BSO_ABSPATH;
    }

    /**
     * Directory name of the plugin
     * @return string
     */
    public static function dirname()
    {
        return BSO_DIRNAME;
    }

    /**
     * Basename of the plugin
     * @return string
     */
    public static function basename()
    {
        return BSO_BASENAME;
    }

    /**
     * Plugin version
     * @return string
     */
    public static function version()
    {
        return BSO_VERSION;
    }

    /**
     * Absolute URL of the plugin
     * @return string
     */
    public static function url()
    {
        return plugins_url(self::dirname());
    }
}