<?php
namespace Blexr\SportOdds\Traits;

/**
 * Trait Slugify
 * @package Blexr\SportOdds\Traits
 */
trait Slugify
{
    /**
     * @return string|string[]
     */
    public function slug()
    {
        return str_replace('/', '-', trim($this->name));
    }
}