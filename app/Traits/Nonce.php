<?php
namespace Blexr\SportOdds\Traits;

/**
 * Trait Nonce
 * @package Blexr\SportOdds\Traits
 */
trait Nonce
{
    /**
     * @return false|string
     */
    public function createNonce()
    {
        return wp_create_nonce($this->nonce);
    }

    /**
     * @return bool|int
     */
    public function CheckAjaxReferer()
    {
        return check_ajax_referer($this->nonce);
    }
}