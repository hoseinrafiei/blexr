<?php
namespace Blexr\SportOdds;

use Jenssegers\Blade\Blade;

/**
 * Class Base
 * @package Blexr\SportOdds
 */
class Base
{
    public function __construct()
    {
    }

    /**
     * Initialize Blade Template Engine
     * @return Blade
     */
    public function view()
    {
        return (new Blade(Plugin::abspath().'/views', Plugin::abspath().'/views/cache'));
    }

    /**
     * Initialize API Class
     * @return Odds
     */
    public function getAPI()
    {
        $options = get_option('blexr_odds_options');
        if(!is_array($options)) $options = [];

        // API Token
        $api_key = (isset($options['api_key']) and trim($options['api_key'])) ? $options['api_key'] : '5bafb073cf4c7a2b810b166cdb7d2c64';

        return (new Odds($api_key));
    }

    /**
     * Send JSON Response
     * @param array $response
     */
    public function JSONResponse(array $response)
    {
        wp_send_json($response);
    }
}