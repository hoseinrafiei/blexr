<?php
namespace Blexr\SportOdds;

/**
 * Class Cache
 * @package Blexr\SportOdds
 */
class Cache extends Base
{
	public function __construct()
    {
        parent::__construct();
	}

    /**
     * A handy wrapper for WordPress transient API
     * @param string $name
     * @param int $expiry
     * @param callable $callback
     * @param string $prefix
     * @return mixed
     */
    public static function get($name, $expiry, $callback, $prefix = 'bs')
    {
        // Create Cache Key with Prefix
        $key = $prefix.'_'.$name;

        $data = get_transient($key);
        if(!$data)
        {
            $data = call_user_func($callback);
            set_transient($key, $data, $expiry);
        }

        return $data;
    }
}