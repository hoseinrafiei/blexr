<?php
namespace Blexr\SportOdds;

use Blexr\SportOdds\Interfaces\Initiable;

/**
 * Class Settings
 * @package Blexr\SportOdds
 */
class Settings extends Base implements Initiable
{
	public function __construct()
    {
        parent::__construct();
	}

    /**
     * Initialize the settings menu
     */
    public function init()
    {
        add_action('admin_menu', [$this, 'menu']);
        add_action('admin_init', [$this, 'register']);
    }

    /**
     * Register the settings section and fields
     */
    public function register()
    {
        register_setting('blexr_odds_options', 'blexr_odds_options', [$this, 'validate']);
        add_settings_section('blexr_odds_api', NULL, function()
        {
            echo '<p>'.sprintf(esc_html__('Please set the API key of %s for connecting to the Odds API.', 'blexr-sport-odds'), '<a href="https://the-odds-api.com/" target="_blank"><strong>https://the-odds-api.com/</strong></a>').'</p>';
        }, 'blexr-sport-odds');

        add_settings_field('blexr_odds_api_key', esc_html__('API Key', 'blexr-sport-odds'), function()
        {
            $options = get_option('blexr_odds_options');
            if(!is_array($options)) $options = [];

            echo '<input id="blexr_odds_api_key" class="widefat" name="blexr_odds_options[api_key]" type="text" value="'.(isset($options['api_key']) ? esc_attr($options['api_key']) : '').'">';
        }, 'blexr-sport-odds', 'blexr_odds_api');
    }

    /**
     * Add settings menu under WordPress -> Settings
     */
    public function menu()
    {
        add_options_page(esc_html__('Blexr Settings', 'blexr-sport-odds'), esc_html__('Blexr Settings', 'blexr-sport-odds'), 'manage_options', 'blexr-sport-odds', [$this, 'output']);
    }

    /**
     * Generate the output of settings menu
     */
    public function output()
    {
        ob_start();

        settings_fields('blexr_odds_options');
        do_settings_sections('blexr-sport-odds');
        $form = ob_get_clean();

        // Generate Table
        echo $this->view()->make('settings', compact('form'))->render();
    }

    /**
     * Validate the options
     * @param array $input
     * @return array
     */
    public function validate(array $input)
    {
        $validated = [];

        $validated['api_key'] = trim($input['api_key']);
        if(!preg_match('/^[a-z0-9]{32}$/i', $validated['api_key'])) $validated['api_key'] = '';

        return $validated;
    }
}