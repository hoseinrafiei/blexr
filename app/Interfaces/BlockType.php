<?php
namespace Blexr\SportOdds\Interfaces;

/**
 * Interface BlockType
 * @package Blexr\SportOdds\Interfaces
 */
interface BlockType
{
    public function hooks();
    public function register();
    public function styles();
    public function scripts();
    public function render();
}