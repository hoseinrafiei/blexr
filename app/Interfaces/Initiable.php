<?php
namespace Blexr\SportOdds\Interfaces;

/**
 * Interface Initiable
 * @package Blexr\SportOdds\Interfaces
 */
interface Initiable
{
    public function init();
}