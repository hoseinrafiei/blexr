<?php
namespace Blexr\SportOdds;

/**
 * Class Odds
 * @package Blexr\SportOdds
 */
class Odds extends Base
{
    /**
     * API Token
     * @var string
     */
    private string $token;

    /**
     * Version 4 of Odds API
     * @var string
     */
    private string $base = 'https://api.the-odds-api.com/v4';

    public function __construct($token)
    {
        parent::__construct();

        $this->token = $token;
	}

    /**
     * Get Odds from the public API based on criteria
     * @param string $sport
     * @param array $regions
     * @param array $markets
     * @return mixed
     */
    public function getOdds($sport, array $regions, array $markets = [])
    {
        $endpoint = $this->endpoint('sports/'.$sport.'/odds');
        $name = 'adds_'.$sport.'_'.implode('_', $regions).'_'.implode('_', $markets);

        return Cache::get($name, HOUR_IN_SECONDS, function() use($endpoint, $regions, $markets)
        {
            if(is_array($regions) and count($regions)) $endpoint .= '&regions='.implode(',', $regions);
            if(is_array($markets) and count($markets)) $endpoint .= '&markets='.implode(',', $markets);

            $response = wp_remote_get($endpoint, [
                'timeout' => 10,
            ]);

            $JSON = wp_remote_retrieve_body($response);

            // No Response
            if(trim($JSON) === '') return [];
            else return json_decode($JSON);
        });
    }

    /**
     * Get Sports from the public API
     * @param bool $all
     * @return mixed
     */
    public function getSports($all = false)
    {
        $endpoint = $this->endpoint('sports');

        return Cache::get('sports_'.($all ? 1 : 0), DAY_IN_SECONDS, function() use($endpoint, $all)
        {
            if($all) $endpoint .= '&all=true';

            $response = wp_remote_get($endpoint, [
                'timeout' => 10,
            ]);

            $JSON = wp_remote_retrieve_body($response);

            // No Response
            if(trim($JSON) === '') return [];
            else return json_decode($JSON);
        });
	}

    /**
     * Get Markets
     * @return array
     */
    public function getMarkets()
    {
        return [
            'h2h' => __('Head to Head / MoneyLine', 'blexr-sport-odds'),
            'spreads' => __('Points Handicaps', 'blexr-sport-odds'),
            'totals' => __('Over / Under', 'blexr-sport-odds'),
            'outrights' => __('Futures', 'blexr-sport-odds'),
        ];
    }

    /**
     * Get Regions
     * @return array
     */
    public function getRegions()
    {
        return [
            'us' => __('United States', 'blexr-sport-odds'),
            'uk' => __('United Kingdom', 'blexr-sport-odds'),
            'au' => __('Australia', 'blexr-sport-odds'),
            'eu' => __('Europe', 'blexr-sport-odds'),
        ];
    }

    /**
     * Generate the full endpoint and append API key to the URL
     * @param string $method
     * @return string
     */
	private function endpoint($method)
    {
        return trim($this->base.'/'.$method.'/?apiKey='.$this->token);
    }
}