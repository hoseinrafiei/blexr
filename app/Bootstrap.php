<?php
namespace Blexr\SportOdds;

use Blexr\SportOdds\Interfaces\Initiable;

/**
 * Class Bootstrap
 * @package Blexr\SportOdds
 */
class Bootstrap extends Base implements Initiable
{
    /**
     * @var string
     */
    public string $version = '1.0.0';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Initialize the Plugin
     */
    public function init()
    {
        // Define Constants
        $this->constants();

        // Check Minimum Requirements
        if(!Plugin::meetRequirements())
        {
            add_action('admin_notices', function()
            {
                echo '<div class="notice notice-error is-dismissible"><p>'.__('PHP 7.4 is needed to run this plugin!', 'blexr-sport-odds').'</p></div>';
            });

            require_once ABSPATH . 'wp-admin/includes/plugin.php';
            deactivate_plugins(Plugin::basename());

            return false;
        }

        // Internationalization (I18n)
        (new I18n())->init();

        // Block Editor
        (new Blocks())->init();

        // Settings
        (new Settings())->init();

        // Plugin Hooks
        (new Plugin())->init();
    }

    /**
     * Define PHP Constants
     */
    private function constants()
    {
        // Absolute Path
        if(!defined('BSO_ABSPATH')) define('BSO_ABSPATH', dirname(__DIR__));

        // Directory Name
        if(!defined('BSO_DIRNAME')) define('BSO_DIRNAME', basename(BSO_ABSPATH));

        // Plugin Base Name
        if(!defined('BSO_BASENAME')) define('BSO_BASENAME', plugin_basename(BSO_ABSPATH.'/blexr-sport-odds.php'));

        // Full Path of Main File
        if(!defined('BSO_FILEPATH')) define('BSO_FILEPATH', BSO_ABSPATH.'/blexr-sport-odds.php');

        // Version
        if(!defined('BSO_VERSION')) define('BSO_VERSION', $this->version);
    }
}