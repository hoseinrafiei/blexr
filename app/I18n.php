<?php
namespace Blexr\SportOdds;

use Blexr\SportOdds\Interfaces\Initiable;

/**
 * Class I18n
 * @package Blexr\SportOdds
 */
class I18n extends Base implements Initiable
{
	public function __construct()
    {
        parent::__construct();
	}

    /**
     * Initialize i18n
     */
    public function init()
    {
        // Register Language Files
        add_action('plugins_loaded', [$this, 'textdomain']);
    }

    /**
     * Register the textdomain and linked language file
     */
    public function textdomain()
    {
        // Get current locale
        $locale = apply_filters('plugin_locale', get_locale(), 'blexr-sport-odds');

        // WordPress language directory /wp-content/languages/blexr-sport-odds-en_US.mo
        $path = WP_LANG_DIR.'/blexr-sport-odds-'.$locale.'.mo';

        // If language file exists on WordPress language directory use it
        if(File::exists($path))
        {
            load_textdomain('blexr-sport-odds', $path);
        }
        // Otherwise use Blexr plugin directory /path/to/plugin/i18n/languages/blexr-sport-odds-en_US.mo
        else
        {
            load_plugin_textdomain('blexr-sport-odds', false, Plugin::abspath().'/i18n/languages/');
        }
    }
}