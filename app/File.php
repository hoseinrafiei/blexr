<?php
namespace Blexr\SportOdds;

/**
 * Class File
 * @package Blexr\SportOdds
 */
class File extends Base
{
	public function __construct()
    {
        parent::__construct();
	}

    /**
     * Read Files
     * @param string $path
     * @return false|string
     */
    public static function read($path)
    {
        return file_get_contents($path);
    }

    /**
     * Check Existence of a file
     * @param string $path
     * @return bool
     */
    public static function exists($path)
    {
        return file_exists($path);
    }

    /**
     * Write to a file
     * @param string $path
     * @param string $content
     * @return false|int
     */
    public static function write($path, $content)
    {
        return file_put_contents($path, $content);
    }

    /**
     * Append new contents at the end of a file
     * @param string $path
     * @param string $content
     * @return false|int
     */
    public static function append($path, $content)
    {
        return file_put_contents($path, $content, FILE_APPEND);
    }

    /**
     * Delete a file
     * @param string $path
     * @return bool
     */
    public static function delete($path)
    {
        return unlink($path);
    }
}