<?php
namespace Blexr\SportOdds;

use Blexr\SportOdds\Blocks\Odds;
use Blexr\SportOdds\Interfaces\Initiable;
use Blexr\SportOdds\Traits\Nonce;
use Blexr\SportOdds\Traits\Slugify;

/**
 * Class Blocks
 * @package Blexr\SportOdds
 */
class Blocks extends Base implements Initiable
{
    use Slugify;
    use Nonce;

    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $nonce;

	public function __construct()
    {
        parent::__construct();
	}

    /**
     * Initialize All Blocks for Block Editor
     */
    public function init()
    {
        // Odds Table
        (new Odds())->hooks();

        // Initialize Rest of Blocks
        // ...

        // Blexr Category
        $this->category();
	}

    /**
     * Add Custom Category for Block Editor
     */
    public function category()
    {
        add_action('block_categories_all', function($categories)
        {
            return array_merge($categories, [[
                'slug'  => 'blexr',
                'title' => esc_html__('Blexr', 'blexr-sport-odds'),
            ]]);
        }, 10, 1);
	}
}