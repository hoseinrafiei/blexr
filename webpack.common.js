const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        "block-odds": "./assets/src/js/block-odds.js",
        "block-odds-editor": "./assets/src/js/block-odds-editor.js",
        "style-frontend": "./assets/src/scss/block-odds.scss",
        "style-editor": "./assets/src/scss/block-odds-editor.scss",
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Production',
        }),
    ],
    output: {
        filename: 'js/[name].min.js',
        path: path.resolve(__dirname, 'assets/dist'),
        clean: true,
    },
    module: {
        rules: [{
            test: /\.scss$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: 'css/[name].css',
                }
            }, {
                loader: 'extract-loader'
            }, {
                loader: 'css-loader'
            }, {
                loader: 'postcss-loader'
            }, {
                loader: 'sass-loader'
            }]
        }]
    }
};