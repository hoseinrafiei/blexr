const { registerBlockType } = wp.blocks;

/**
 * Odds Table Block
 */
registerBlockType(blexr.name, {
    title: blexr.title,
    category: 'blexr',
    icon: 'editor-table',
    description: blexr.description,
    keywords: ['odds', 'table', 'blexr', 'bet'],
    edit: () => {
        return blexr.title;
    },
    save: () => {
        return null;
    }
});