// Blexr Odds Table Plugin
(function($)
{
    $.fn.BlexrOddsTable = function(options)
    {
        // Default Options
        let settings = $.extend(
        {
            // These are the defaults.
            id: 0,
            ajax_url: '',
        }, options);

        // Wrapper
        let $wrapper = $("#bs-odds-"+settings.id);
        let $form = $wrapper.find('.bs-odds-filter-form');
        let $table = $wrapper.find('.bs-odds-table-wrapper');
        let $message = $wrapper.find('.bs-odds-message');

        // Set the listener
        setListeners();

        function setListeners()
        {
            // Form Submit
            $form.on('submit', function(e)
            {
                e.preventDefault();

                filter();
            }).trigger('submit');
        }

        function filter()
        {
            // Button
            let $button = $form.find('button');

            // Add loading Class
            $table.html('<div class="bs-loading"><div></div><div></div></div>');

            // Disable the Button
            $button.prop('disabled', 'disabled');

            // Remove the Message
            $message.html('');

            // Serialized Form Data
            let data = $form.serialize();

            $.ajax(
            {
                url: settings.ajax_url,
                data: data,
                dataType: "json",
                type: "get",
                success: function(response)
                {
                    // Enable the Button
                    $button.prop('disabled', false);

                    if(response.success)
                    {
                        // Display Items
                        $table.html(response.output);
                    }
                    else
                    {
                        // Display Error
                        $message.html(response.message);
                    }
                },
                error: function(request)
                {
                    // Remove Loading Style
                    $table.html('');

                    // Enable the Button
                    $button.prop('disabled', false);

                    // Display Error
                    $message.html(request.responseText);
                }
            });
        }
    };
}(jQuery));
