# Getting Started
---
To use this plugin PHP 7.4 and higher is required.

- To install the plugin, just clone the repository on `/wp-content/plugins/` directory and activate it using WordPress -> Plugins menu just like other normal plugins.
- Then create a page and insert the `Odds Table` block into the page. You can find it by searching `Odds`, `Blexr` or checking **BLEXR** category.
- The plugin by default use my own token but if you like to change the API token, you can use the plugin settings menu under WordPress Settings menu.
- If you want to run the webpack commands, please install the dependencies by running `npm install` command.

# Developments
---
I used following `technologies` and did the following `developments`, you can check them by browsing the code.

- PHP Composer with `PSR4` Auto-loading.
- Webpack for JS and SCSS compile and added following 4 commands.
    - `dev` to compile the assets for development purposes.
    - `dev-watch` to watch the files and compile them in case of changes for development purposes.
    - `prod` to compile the assets for production purposes.
    - `prod-watch` to watch the files and compile them in case of changes for production purposes.
- Added 3 different files for webpack configuration.
- Blade template engine by installing `jenssegers/blade` package.
- Configured `blade` engine to store the rendered files into a cache directory.
- Added language files for the plugin to make it translation and multilingual ready.
- Added `src` and `dist` directories to keep source and compiled JS and CSS files.
- Used WordPress `Settings API` to add a settings page.
- Used WordPress `Transient API` to speed up the website.
- Added a handy wrapper for transient API to make the code more cleaner. You can find it in `app/Cache.php` file.
- Used `OOP` features of PHP as much as possible including `final` keyword, `Inheritence`, `Interface`, `Trait`, etc.
- Used `Namespacing`.
- Add different JS and CSS files for `block editor` and `WP frontend`.
- Added a PHP File class to wrap some PHP functions. You can find it in `app/File.php` path.
- Added a custom category for our blocks in block editor named `BLEXR`.
- Avoid using CSS frameworks and used `pure CSS` codes.
- Clear `commenting` on the codes.
- Used `markdown` to write this instruction.
- Used `git` and `bitbucket` to publish this plugin.
- Used `ES6` to write JS codes of block component.
- Added `responsive` form and table as the content of block.
- Used a nice `loading style` with CSS `animation` when the form is loading new results.
- Used `AJAX` to improve the `UX` and speed of plugin.
- Used `WordPress Nonce` and `Referer Check` to improve the security.
- Added a `jQuery Plugin` to have a clean and `modular` JS codes for frontend.
- Developed an ability to have `multiple instance` of block on same page without ID conflict or JS errors. You can add two instance of Odds Table block to single page and both works independently.
- Added `activation`, `deactivation`, and `uninstallation` hooks for the plugin.
- Removed plugin options on un-installation.
- Used `WordPress Options` API.