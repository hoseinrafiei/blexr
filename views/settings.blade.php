<div class="wrap about-wrap">
    <h1>{{ esc_html__('Blexr Settings', 'blexr-sport-odds') }}</h1>
    <form action="options.php" method="post">
        {!! $form !!}
        <input name="submit" class="button button-primary button-hero" type="submit" value="{{ esc_attr__('Save', 'blexr-sport-odds') }}">
    </form>
</div>