@if(is_array($odds) and count($odds))
<ul>
    @foreach($odds as $odd)
        @continue(!$odd->home_team)
    <li>
        <div class="bs-odds-teams">
            <div class="bs-odds-teams-home"><img src="{{ esc_url(\Blexr\SportOdds\Plugin::url().'/assets/img/home.png') }}" alt="<?php esc_attr_e('Home', 'blexr-sport-odds'); ?>">{{ $odd->home_team }}</div>
            <div class="bs-odds-teams-vs">vs</div>
            <div class="bs-odds-teams-away">{{ $odd->away_team }}</div>
            <div class="bs-odds-commence-time">{{ (new DateTime($odd->commence_time))->format('F j, Y - g:i a') }}</div>
        </div>

        @if(isset($odd->bookmakers) and is_array($odd->bookmakers) and count($odd->bookmakers))
        <div class="bs-odds-bookmarkers">
            <ul>
                @foreach($odd->bookmakers as $bookmarker)
                <li>
                    <div class="bs-odds-bookmarker-title">{{ $bookmarker->title }}</div>
                    <ul class="bs-odds-outcomes">
                        @foreach($bookmarker->markets[0]->outcomes as $outcome)
                        <li>
                            <div class="bs-odds-outcomes-number">{{ $loop->iteration }}</div>
                            <div class="bs-odds-outcomes-price">{{ $outcome->price }}</div>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </li>
    @endforeach
</ul>
@else
<p class="bs-alert bs-warning"><?php esc_html_e('Nothing found! Please try another sport or region ...', 'blexr-sport-odds'); ?></p>
@endif