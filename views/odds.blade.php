<div class="bs-odds-wrapper" id="bs-odds-{{ $uid }}" data-id="{{ $uid }}">
    <div class="bs-odds-filter-wrapper">
        <form class="bs-odds-filter-form">
            <div class="bs-odds-filter-control">
                <label for="bs-odds-filter-sports-{{ $uid }}"><?php esc_html_e('Sports', 'blexr-sport-odds'); ?></label>
                <select name="sport" id="bs-odds-filter-sports-{{ $uid }}">
                    @foreach($sports as $sport)
                    <option value="{{ $sport->key }}">{{ $sport->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="bs-odds-filter-control">
                <label for="bs-odds-filter-regions-{{ $uid }}"><?php esc_html_e('Regions', 'blexr-sport-odds'); ?></label>
                <select name="region" id="bs-odds-filter-regions-{{ $uid }}">
                    @foreach($regions as $region => $label)
                    <option value="{{ $region }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>
            <div class="bs-odds-filter-control">
                <label for="bs-odds-filter-markets-{{ $uid }}"><?php esc_html_e('Markets', 'blexr-sport-odds'); ?></label>
                <select name="market" id="bs-odds-filter-markets-{{ $uid }}">
                    @foreach($markets as $market => $label)
                    <option value="{{ $market }}">{{ $label }}</option>
                    @endforeach
                </select>
            </div>
            <div class="bs-odds-filter-control">
                {!! wp_nonce_field($nonce, '_wpnonce', true, false) !!}
                <input type="hidden" name="action" value="bs_ajax_odds">
                <label>&nbsp;</label>
                <button type="submit" class="button"><?php esc_html_e('Filter', 'blexr-sport-odds'); ?></button>
            </div>
        </form>
    </div>
    <div class="bs-odds-message bs-alert bs-error"></div>
    <div class="bs-odds-table-wrapper"></div>
</div>