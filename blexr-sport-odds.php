<?php
/*
    Plugin Name: Blexr Sports Odds Table
    Plugin URI: https://blexr.com/
    Description: Blexr Development Task
    Author: Hossein Rafiei <hoseinrafiei@gmail.com>
    Version: 1.0.0
    Text Domain: blexr-sport-odds
    Domain Path: /i18n/languages
    Author URI: https://www.linkedin.com/in/hossein-rafiei/
*/

// Auto loading
require 'vendor/autoload.php';

$bootstrap = new \Blexr\SportOdds\Bootstrap();
$bootstrap->init();